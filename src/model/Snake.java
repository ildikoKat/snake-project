/*
    Katona Ildiko-Noemi
    522-es csoport
    kiim1632
 */

package model;

import java.awt.*;
import java.util.ArrayList;

public class Snake{

    /**
     * The snake that needs to chase the apple to get points
     */

    private int direction; //1 up; 2 right; 3 down; 4 left
    private ArrayList<Tile> body;
    private int width;
    private int height;

    /**
     * Creates a new snake of size initialSize in the corner of the gamePanel with the direction 2 (right)
     * @param w The width of a snake body part
     * @param h The height of a snake body part
     * @param borderW The width of the border
     * @param borderH The height of the border
     */
    public Snake(int w, int h, int borderW, int borderH){
        int initialSize = 3;
        width = w;
        height = h;
        body = new ArrayList<>();
        Color color;
        for(int i = 0; i < initialSize; i++ ){
            if(i ==0)
                color = new Color(0,0,200); //the color of the head
            else
                color = new Color(0,200,0);
            body.add(new Tile(initialSize*borderW - i * width, borderH, width, height, color)); //50 because of the border
        }
        direction = 2;
    }

    public void add(Graphics g) {
        for(int i = 0; i < body.size(); i++){
            body.get(i).paintComponent(g);
        }
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    /**
     * Adds a new tile to the body
     */
    public void addBody(){
        Tile t = new Tile(getTileX(body.size()-1),getTileY(body.size()-1),width,height, new Color(0,200,0));
        body.add(t);
    }

    /**
     * Returns the x coordinate of the head
     * @return The x coordinate of the head
     */
    public int getHeadx(){
        return body.get(0).getX();
    }

    /**
     * Returns the y coordinate of the head
     * @return The y coordinate of the head
     */
    public int getHeady(){
        return body.get(0).getY();
    }

    /**
     * Returns the x coordinate of a body
     * @param i The index of the body
     * @return int The x coordinate of the body of index i
     */
    private int getTileX(int i){return body.get(i).getX(); }

    /**
     * Returns the y coordinate of a body
     * @param i The index of the body
     * @return int The y coordinate of the body of index i
     */
    private int getTileY(int i){return body.get(i).getY(); }

    /**
     * Returns the tile of an index
     * @param i The index of the body
     * @return Tile The tile of the body of index i
     */
    private Tile getTile(int i){return body.get(i);}

    /**
     * Returns the tile of the head
     * @return Tile The tile of the head
     */
    private Tile getHead(){ return body.get(0);
    }

    /**
     * Moves the snake according the direction
     */
    public void move(){

        switch (direction){
            case 1:{
                changeTail(0, -height);
                break;
            }
            case 2: {
                changeTail(width, 0);
                break;
            }
            case 3: {
                changeTail(0, height);
                break;
            }
            case 4: {
                changeTail(-width, 0);
                break;
            }
        }
    }

    /**
     * Modifies the x and y coordinates of the whole snake
     * @param x The value the x coordinate needs to be changed
     * @param y The value the y coordinate needs to be changed
     */
    private void changeTail(int x, int y){
        for(int i=body.size()-1; i>=1; i--){
            getTile(i).setX(getTileX(i-1));
            getTile(i).setY(getTileY(i-1));
        }

        int hx = getHeadx() + x;
        int hy = getHeady() + y;
        getHead().setX(hx);
        getHead().setY(hy);
    }

    /**
     * Checks whether the head of the snake's head is on the the ground
     * @param ground The wall we want to check of collision
     * @return boolean Returns true is the snake collided with the Ground, othervise returns false
     */
    public boolean collide(Ground ground){
        return (getHeadx() < ground.getBorderUp() || getHeadx() >= ground.getBorderDown()
                || getHeady() < ground.getBorderUp() || getHeady() >= ground.getBorderDown());
    }

    /**
     * Checks whether the head of snake's head is on a body part
     * @return boolean Returns true id the snake collided with itself
     */
    public boolean collideSelf(){
        for(int i = 1; i<body.size(); i++){
            if(getTileX(i) == getHeadx() && getTileY(i)== getHeady()){
                return true;
            }
        }
        return false;
    }

    /**
     * Checks whether the parameter tile is on the body
     * @param t The tile which could be on the snake
     * @return boolean Return true if the tile is on the snake
     */
    public boolean collideBody(Tile t){
        for(Tile i : body){
            if(i.getX()==t.getX() && i.getY()==t.getY())
                return true;
        }
        return false;
    }


}