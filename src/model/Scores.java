/*
    Katona Ildiko-Noemi
    522-es csoport
    kiim1632
 */

package model;

import java.io.*;

public class Scores {

    /**
     *  Opens the file that contains the scores and adds a new NAME SCORE to it
     */

    private static final String fileName = "highscore.txt";
    private String name;
    private int score;
    public Scores(String name, int score){
        this.name = name;
        this.score = score;
        if(score!=0) addScore();
    }

    /**
     * Writes the name and the scores to the file
     */
    public void addScore(){
        BufferedWriter writer = null;
        try {
            String output = name + " "+ score;
            writer = new BufferedWriter(new FileWriter(new File(fileName), true));
            writer.write(output);
            writer.newLine();
        } catch (Exception e) {
            System.out.println("Fail buffered writer!" +e);
        }
        finally {
            try {
                writer.close();
            } catch (Exception e) {
                System.out.println("Fail buffered writer!" +e);
            }
        }
    }


}
