/*
    Katona Ildiko-Noemi
    522-es csoport
    kiim1632
 */

package model;

import java.awt.*;

public class Ground {

    /**
     * The game area. The playable zone is surrounded with a border
     */
    private Tile ground[][];
    private int width;
    private int height;
    private int cols;
    private int rows;

    /**
     * Creates the game area
     * @param r The number of rows of the game area
     * @param c The number of columns of the game area
     * @param w The width of the area tiles
     * @param h The height of the area tiles
     */
    public Ground(int r, int c, int w, int h){
        cols = c;
        rows = r;
        width = w;
        height = h;
        ground = new Tile[cols][rows];
        for( int yy = 0; yy < rows; yy++) {
            for(int xx = 0; xx < cols; xx++ ){
                if(yy == 0 || yy == rows -1 || xx==0 || xx == cols-1)  {
                    ground[xx][yy] = new Tile(xx * width, yy * height, width, height, new Color(155, 80, 5) );
                }
                else
                    ground[xx][yy] = new Tile(xx * width, yy * height, width, height, new Color(11, 0, 23) );
            }
        }
    }

    public void add(Graphics g) {
        for(int xx = 0; xx<cols; xx++){
            for(int yy=0; yy<rows; yy++) {
                ground[xx][yy].paintComponent(g);
            }
        }
    }

    public int getBorderRight(){
        return width*(rows -1);
    }

    public int getBorderLeft(){
        return width;
    }

    public int getBorderUp(){
        return height;
    }

    public int getBorderDown(){
        return height*(cols -1);
    }

}