/*
    Katona Ildiko-Noemi
    522-es csoport
    kiim1632
 */

package model;

import javax.swing.*;
import java.awt.*;

public class Tile extends JPanel {

    /**
     * The component of which the snake, wall, and the apple are composed of
     */
    private int x;
    private int y;
    private int width;
    private int height;
    private Color color;

    public Tile(int x, int y, int w, int h, Color c){
        this.x = x;
        this.y = y;
        width = w;
        height = h;
        color = c;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(color);
        g.fillRect(x,y, width, height);
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }


}
