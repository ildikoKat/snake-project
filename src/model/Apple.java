/*
    Katona Ildiko-Noemi
    522-es csoport
    kiim1632
 */

package model;

import java.awt.*;

public class Apple {

    /**
     * The apple that's needed to be eaten by the snake to gain points
     */

    private Tile apple;

    @SuppressWarnings("checkstyle:WhitespaceAround")
    public Apple(int h, int w){
        int x = 25*10;
        int y = 25*10;
        apple = new Tile(x,y,w,h, Color.red);
    }

    public void setX(int x) {
        apple.setX(x);
    }

    public int getX() {
        return apple.getX();
    }

    public int getY() {
        return apple.getY();
    }

    public void setY(int y) {
        apple.setY(y);
    }

    public void add(Graphics g){
        apple.paintComponent(g);
    }


}
