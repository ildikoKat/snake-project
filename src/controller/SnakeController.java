/*
    Katona Ildiko-Noemi
    522-es csoport
    kiim1632
 */

package controller;

import view.GameView;
import model.Scores;
import view.GameFrame;

public class SnakeController implements Runnable{

    /**
     * The link between the GameView and the GameFrame
     */

    private GameView gameView;
    private String playerName;

    /**
     * Creates the gameView with a GameFrame p
     * @param fr The game's frame
     * @param name The player's username
     */
    public SnakeController(GameFrame fr, String name) {
        gameView = new GameView(fr);
        playerName = name;
    }

    /**
     * Returns the game view
     * @return gameView The game panel of the game
     */
    public GameView getControllerGame(){
        return gameView;
    }

    /**
     * The cicle of the game
     */
    @Override
    public void run() {
            while (gameView.isRunning()) {
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    System.out.println("Error sleep: " + e);
                }
                gameView.getGameSnake().move();

                if (gameView.eatApple()) {
                    gameView.incGameScore();
                    gameView.relocateApple();
                    gameView.getGameSnake().addBody();
                }

                if (gameView.getGameSnake().collide(gameView.getGameWall()) || gameView.getGameSnake().collideSelf()) {
                    gameView.setRunning(false);
                    gameView.newGameSnake();
                    gameView.relocateApple();
                }
                gameView.repaint();
            }
            addScore();


    }

    /**
     * Adds the player's name and score to the scores
     */
    public void addScore() {
        Scores score = new Scores(playerName, gameView.getGameScore());
    }


}