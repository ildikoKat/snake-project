/*
    Katona Ildiko-Noemi
    522-es csoport
    kiim1632
 */

package controller;

import view.GameFrame;
import view.HighScoreFrame;
import view.MenuFrame;

public class FrameManager {

    /**
     * Manages the switch between frames, and creates new ones when needed
     */

    private GameFrame gameFrame;
    private MenuFrame menuFrame;
    private String playerName;
    private HighScoreFrame highScoreFrame;


    public FrameManager(){
        menuFrame = new MenuFrame(this);
    }

    /**
     * Sets the menuFrame visible or invisible according to the given parameter
     * @param visible Can be either True or False
     */
    public void setMenuFrameVisibility( boolean visible){
        menuFrame.setVisible(visible);
    }

    /**
     * Creates a new GameFrame
     */
    public void newGameFrame(){
        gameFrame = new GameFrame(this);
    }

    /**
     * Creates a new HighScoreFrame
     */
    public void newHighScoreFrame(){
        highScoreFrame = new HighScoreFrame(this);
    }


    /**
     * Returns the player's username
     * @return playerName
     */
    public String getPlayerName(){
        return playerName;
    }

    /**
     * Set's the player's name to the parameter
     * @param player The player's new username
     */
    public void setPlayerName(String player){
        playerName = player;
    }


}
