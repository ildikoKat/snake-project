/*
    Katona Ildiko-Noemi
    522-es csoport
    kiim1632
 */

package view;

import controller.FrameManager;

import javax.swing.*;

public class MenuFrame extends JFrame {

    /**
     * The menu frame which contains the menu
     * @param frameManager The manager to switch between frames
     */

    public MenuFrame(FrameManager frameManager){
        setTitle("The SnakeGame Menu");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(new Menu(frameManager));
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }

}
