
/*
    Katona Ildiko-Noemi
    522-es csoport
    kiim1632
 */
package view;

import model.Apple;
import model.Ground;
import model.Snake;
import model.Tile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class GameView extends JPanel implements KeyListener{
    /**
     * The view of the whole game and is responsible for the key input
     */
    private static final int rows =24;
    private static final int cols = 24;

    private static final int width = 600;
    private static final int height = 600;
    private static final int tileW = 25; //width/rows;
    private static final int tileH = 25; //height/cols;

    private Ground ground;
    private Snake snake;
    private Apple apple;
    private int score;
    private boolean running;
    private GameFrame gameFrame;

    /**
     * Creates the game's representation
     * @param fr The gamFrame to which to send the signal when to create a new game
     */
    public GameView(GameFrame fr) {
        setPreferredSize(new Dimension(width,height));
        gameFrame = fr;

        ground = new Ground(rows, cols, tileW, tileH);
        snake = new Snake(tileW, tileH,tileW,tileH);
        apple =new Apple(tileW,tileH);
        score = 0;

        running = true;
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public int getGameScore(){
        return score;
    }

    public void incGameScore(){
        score++;
    }

    public Ground getGameWall(){
        return ground;
    }

    public Snake getGameSnake(){
        return snake;
    }

    public void newGameSnake(){
        snake = new Snake(tileW,tileH,tileW, tileH);
    }

    public boolean eatApple() {
        return (snake.getHeadx() == apple.getX() && snake.getHeady() == apple.getY());
    }

    /**
     * Sets the apple to a new location without placing it on the wall or snake
     */
    public void relocateApple(){
        Random r = new Random();
        int x,y;
        do{
            x = r.nextInt(cols-2)*tileW+tileW; //cols-2 so the apple won't be on the border
            y = r.nextInt(rows-2)*tileH+tileH; //rows-2 so the apple won't be on the border
        }while(snake.collideBody(new Tile(x,y,tileW,tileH,Color.gray)));
        apple.setX(x);
        apple.setY(y);
    }

    @Override
    public void paintComponent(Graphics g){
            super.paintComponent(g);
            ground.add(g);
            snake.add(g);
            apple.add(g);
            g.setColor(Color.WHITE);
            g.drawString("Scores:" + score, 25, 25);
        if(!running){
            g.setColor(Color.WHITE);
            Font font = new Font("Helvetica",Font.BOLD,20);
            g.setFont(font);
            g.drawString("GAME OVER! Scores: "+score, 25*7, 25*10);
            font = new Font("Helvetica", Font.ITALIC,14);
            g.setFont(font);
            g.drawString(" <Press SPACEBAR to restart>", 25*7, 25*11);
            g.drawString(" < Or close window to go to the menu>", 25*7, 25*12);
            score = 0;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    /**
     * Sets the snake's direction according to the arrow keys pressed. If the space is pressed while the game isn't running, it restarts
     * @param e The pressed key event
     */
    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_SPACE && !running){
                gameFrame.restartGame();
                setRunning(true);
        }

        if(running) {
            int direction = snake.getDirection();
            switch (key) {
                case KeyEvent.VK_UP: {
                    if (direction != 3) {
                        snake.setDirection(1);
                    }

                    break;
                }
                case KeyEvent.VK_RIGHT: {
                    if (direction != 4) {
                        snake.setDirection(2);
                    }
                    break;
                }
                case KeyEvent.VK_DOWN: {
                    if (direction != 1) {
                        snake.setDirection(3);
                    }
                    break;
                }
                case KeyEvent.VK_LEFT: {
                    if (direction != 2) {
                        snake.setDirection(4);
                    }
                    break;
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }


}