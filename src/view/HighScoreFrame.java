/*
    Katona Ildiko-Noemi
    522-es csoport
    kiim1632
 */

package view;

import controller.FrameManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;

public class HighScoreFrame extends JFrame {

    /**
     * The frame that contains the high scores
     */

    private static final String fileName = "highscore.txt";

    private TextArea scoreField;
    private static final int width =250;
    private static final int height=200;

    /**
     * The constructor for a new HighScore frame, that contains the names and scores in the fileName
     * @param frameManager The manager that controls the frames
     */
    public HighScoreFrame(FrameManager frameManager){
        setTitle("High Scores");
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) { //When closed, sets the manu frame visible
                super.windowClosing(e);
                setVisible(false);
                dispose();
                frameManager.setMenuFrameVisibility(true);
            }
        });

        JPanel highScorePanel = new JPanel();
        highScorePanel.setBackground(new Color(204, 249, 176));
        highScorePanel.setLayout(new FlowLayout());
        highScorePanel.setPreferredSize(new Dimension(width, height));
        scoreField = new TextArea(10,20);
        scoreField.setEditable(false);
        scoreField.setSize(width,height);
        highScorePanel.add(scoreField, BorderLayout.CENTER);
        listScore();

        add(highScorePanel);

        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }

    /**
     * Opens the file with the scores and writes them into the text area
     */
    public void listScore(){
        try (BufferedReader br = new BufferedReader(new FileReader(new File(fileName)))) {
            String line;
            while ((line = br.readLine()) != null) {
                scoreField.append(line);
                scoreField.append("\n");
            }
        } catch (Exception e) {
            System.out.println("Fail buffered reader!" +e);
        }

    }


}
