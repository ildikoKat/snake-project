/*
    Katona Ildiko-Noemi
    522-es csoport
    kiim1632
 */

package view;

import controller.FrameManager;
import controller.SnakeController;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GameFrame extends JFrame {

    /**
     * The holds the game's representation
     */

    private SnakeController snakeController;

    public GameFrame(FrameManager frameManager){
        setTitle("TheSnakeGame");
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                setVisible(false);
                dispose();
                frameManager.setMenuFrameVisibility(true);
            }
        });

        snakeController = new SnakeController(this, frameManager.getPlayerName());
        add(snakeController.getControllerGame());
        Thread th = new Thread(snakeController);
        th.start();

        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }

    /**
     * Creates a new thread that restarts the game
     */
    public void restartGame(){
        Thread th = new Thread(snakeController);
        th.start();
    }


}
