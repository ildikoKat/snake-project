/*
    Katona Ildiko-Noemi
    522-es csoport
    kiim1632
 */

package view;

import controller.FrameManager;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

public class Menu extends JPanel implements ActionListener {

    /**
     * The representation of the menu
     */

    private JButton newGame;
    private JButton highScores;
    private JButton exit;
    private BufferedImage image;
    private JLabel menuImage;
    private FrameManager frameManager;

    private static final int buttonW = 50;
    private static final int buttonH = 20;
    private int width;
    private int height;

    /**
     * Creates the menu with it's buttons
     * @param frameManager The manager to change between frames
     */

    public Menu(FrameManager frameManager) {
        this.frameManager = frameManager;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBackground(new Color(204, 249, 176));

        newGame = new JButton("New Game");
        newGame.setSize(buttonW, buttonH);
        highScores = new JButton("High Scores");
        highScores.setSize(buttonW, buttonH);
        exit = new JButton("Exit");
        exit.setSize(buttonW, buttonH);

        try {
            image = ImageIO.read(new File("snakePicture.png"));
        } catch (Exception e) {
            System.out.println("Error! Couldn't open image! " + e);
        }

        menuImage = new JLabel(new ImageIcon(image));

        width = image.getWidth() + 100;
        height = image.getHeight() + 3 * buttonH + 30;

        menuImage.setAlignmentX(CENTER_ALIGNMENT);
        newGame.setAlignmentX(CENTER_ALIGNMENT);
        highScores.setAlignmentX(CENTER_ALIGNMENT);
        exit.setAlignmentX(CENTER_ALIGNMENT);

        newGame.addActionListener(this);
        highScores.addActionListener(this);
        exit.addActionListener(this);

        add(menuImage);
        add(newGame);
        add(highScores);
        add(exit);

        setPreferredSize(new Dimension(width, height));
    }

    /**
     * The actions h=when the buttons are pressed
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == newGame) {
            /*
                If the newGame button was pressed, a dialog appears to get the player's username and opens the gameFrame
             */
            String name ="";
            name = JOptionPane.showInputDialog("Your username:");
            while((name != null) && (name.length() == 0)){
                    name = JOptionPane.showInputDialog("Your username:");
            }

            if(name != null) {
                frameManager.setPlayerName(name);
                frameManager.setMenuFrameVisibility(false);
                frameManager.newGameFrame();
            }
        }

        if (e.getSource() == highScores) {
            /*
                If the highScore button was pressed, it opens the highScoreFrame
             */
            frameManager.setMenuFrameVisibility(false);
            frameManager.newHighScoreFrame();
        }
        if (e.getSource() == exit) {
            System.exit(0);
        }
    }


}